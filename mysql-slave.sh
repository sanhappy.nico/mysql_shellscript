#!/bin/bash

###################################
#Description: Install  MySQL_Slave
#Date:        2022-01-22
#Os:          CentOS 7
#Author:      Nicolas
#Run user     root
###################################


#安裝基礎套件
yum -y install epel-release  
yum -y install vim net-tools telnet htop wget git zip unzip bind-utils bash-completion  

#關閉系統防火牆與Selinux
systemctl stop firewalld
systemctl disable firewalld
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

#下載MySQL5.7.34源碼包
cd /tmp
wget https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.37-linux-glibc2.12-x86_64.tar
tar -xf mysql-5.7.37-linux-glibc2.12-x86_64.tar
tar -zxvf mysql-5.7.37-linux-glibc2.12-x86_64.tar.gz

#建立data目錄
mkdir /data
#將解壓mysql移動到data目錄下
mv /tmp/mysql-5.7.37-linux-glibc2.12-x86_64 /data/mysql

#建立MySQL使用者與群組
groupadd mysql
useradd -r -g mysql mysql

#創建設定檔、資料夾
mkdir -p /data/mysql/logs
mkdir -p /data/mysql/mysqld
mkdir -p /data/mysql/binlog
mkdir -p /data/mysql/data
mkdir -p /etc/my.cnf.d

#設定MySQL.conf_Slave
cat > /etc/my.cnf << "EOF"
[client]
#默認連接端口
port = 3306
#用於本地連接的socket套接字
socket = /data/mysql/mysqld/mysql.sock
[mysql]
#設置mysql客戶端默認字符集
default-character-set=utf8
[mysqld]
#使用mysql用戶啟動
user=mysql
#跳過權限表校驗
#skip-grant-tables
skip-name-resolve
#設置3306端口
port = 3306
#設置mysql的安裝目錄
basedir=/data/mysql
#設置mysql數據庫的數據的存放目錄
datadir=/data/mysql/data
#socket文件是在Linux/Unix環境下特有的，用戶在Linux/Unix環境下客戶端連接可以不通過TCP/IP網絡而直接使用unix socket連接MySQL。
socket=/data/mysql/mysqld/mysql.sock
#允許最大連接數
max_connections=200
#服務端使用的字符集默認為8比特編碼的latin1字符集
character-set-server=utf8
#創建新表時將使用的默認存儲引擎
default-storage-engine=INNODB
#表名存儲在磁盤是小寫的，但是比較的時候是不區分大小寫
lower_case_table_names=1
#服務所能處理的請求包的最大大小以及服務所能處理的最大的請求大小
max_allowed_packet=16M
#是否支持快捷方式
symbolic-links=0
#錯誤日誌路徑
log-error=/data/mysql/logs/mysqld.log
#pid文件所在目錄
pid-file=/data/mysql/mysqld/mysqld.pid
server-id = 2 # 表示唯一的 Server ID，串連主機中 Server ID 不可重複。
sync_binlog = 0 #剛剛有提到，為了加速同步的速度，可以關閉這個選項。但是如果您的架構是需要主備切換 (兩台都是 Master 也同時是 Slave)，那麼就需要開啟這個選項，確保 Transaction 寫入到磁碟中。
read_only = 1 # 由於我們的 Slave 只是用來分散 Query，為了怕有笨蛋把資料寫過來，啟動「唯讀」是比較保險的作法。
log-slave-updates = 0 # 如果開啟這個功能表示從其他 Master 讀到的 Binlog 也要寫進自己的 Binlog，這樣自己才能也變成 Master 讓其他 Slave 節點同步，像是有三台 Master/Slave 循環複製實就會需要使用。
#slave二進位記錄檔
relay-log=/data/mysql/binlog/relay-log-bin
relay-log-index=/data/mysql/binlog/slave-relay-bin.index
EOF


#將data/mysql的擁有者及群組改為MySQL
chown -R mysql.mysql /data/mysql

#mysql初始化
/data/mysql/bin/mysqld --initialize --user=mysql --basedir=/data/mysql --datadir=/data/mysql/data


#設定環境變數
cat >> /etc/profile << "EOF"
MYSQL_HOME=/data/mysql
PATH=$PATH:$MYSQL_HOME/bin
EOF

###################################################################################################
#將MySQL寫進系統服務
cat > /usr/lib/systemd/system/mysqld.service << "EOF"
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=http://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql

Type=forking

PIDFile=/data/mysql/mysqld/mysqld.pid

# Disable service start and stop timeout logic of systemd for mysqld service.
TimeoutSec=0

# Execute pre and post scripts as root
PermissionsStartOnly=true

# Start main service
ExecStart=/data/mysql/bin/mysqld --daemonize --pid-file=/data/mysql/mysqld/mysqld.pid $MYSQLD_OPTS

# Sets open_files_limit
LimitNOFILE = 5000

Restart=on-failure

RestartPreventExitStatus=1

PrivateTmp=false
EOF

systemctl daemon-reload
###################################################################################################

#啟動 MySQL
systemctl start mysqld.service

#預設開機啟動 MySQL
systemctl enable mysqld.service


#slave
echo "0、請手動輸入 source /etc/profile 套用環境變數"
echo "請手動輸入 啟動 MySQL systemctl start mysqld.service"
echo "請手動輸入 預設開機啟動 MySQL systemctl enable mysqld.service"
echo "1、手動查看MySQL root預設密碼，請登入${user_name}身份"
echo "cat /data/mysql/logs/mysqld.log | grep "\"temporary password"\" "
echo "2、透過mysql -u root -p 登入，輸入舊密碼"
echo "3、變更密碼alter user 'root'@'localhost' identified by '輸入password';" 
echo "4、變更密碼後請exit退出後再重新登入" 
echo "5、設定master IP位址與查詢的值 change master to master_host='輸入master-ip',master_user='myslave',master_password='輸入password',master_log_file='master-bin.xxxxxx',master_log_pos=xxxx; "
echo "6、啟動 start slave;"
echo "7、show slave status \G # 檢視slave確保兩個值都為yes"
echo "8、給伺服器授權 flush privileges;"
echo "9、slave 設定完成後 驗證是否一致show databases;"
echo "10、確認無誤後exit即可退出"

echo "出現Slave_IO_Running: No的機器上操作：優先檢查mysql-bin名稱
mysql > stop slave;
mysql > CHANGE MASTER TO MASTER_LOG_FILE='mysql-bin.000026', MASTER_LOG_POS=0;  
mysql > start slave;
mysql > show slave status\G"
