#!/bin/bash

###################################
#Description: Install  MySQL_Master
#Date:        2022-01-22
#Os:          CentOS 7
#Author:      Nicolas
#Run user     root
###################################


#安裝基礎套件
yum -y install epel-release  
yum -y install vim net-tools telnet htop wget git zip unzip bind-utils bash-completion  

#關閉系統防火牆與Selinux
systemctl stop firewalld
systemctl disable firewalld
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

#下載MySQL5.7.34源碼包
cd /tmp
wget https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.37-linux-glibc2.12-x86_64.tar
tar -xf mysql-5.7.37-linux-glibc2.12-x86_64.tar
tar -zxvf mysql-5.7.37-linux-glibc2.12-x86_64.tar.gz

#建立data目錄
mkdir /data
#將解壓mysql移動到data目錄下
mv /tmp/mysql-5.7.37-linux-glibc2.12-x86_64 /data/mysql

#建立MySQL使用者與群組
groupadd mysql
useradd -r -g mysql mysql

#創建設定檔、資料夾
mkdir -p /data/mysql/logs
mkdir -p /data/mysql/mysqld
mkdir -p /data/mysql/binlog
mkdir -p /data/mysql/data
mkdir -p /etc/my.cnf.d

#設定MySQL.conf_Master
cat > /etc/my.cnf << "EOF"
[client]
#默認連接端口
port = 3306
#用於本地連接的socket套接字
socket = /data/mysql/mysqld/mysql.sock
[mysql]
#設置mysql客戶端默認字符集
default-character-set=utf8
[mysqld]
#使用mysql用戶啟動
user=mysql
#跳過權限表校驗
#skip-grant-tables
skip-name-resolve
#設置3306端口
port = 3306
#設置mysql的安裝目錄
basedir=/data/mysql
#設置mysql數據庫的數據的存放目錄
datadir=/data/mysql/data
#socket文件是在Linux/Unix環境下特有的，用戶在Linux/Unix環境下客戶端連接可以不通過TCP/IP網絡而直接使用unix socket連接MySQL。
socket=/data/mysql/mysqld/mysql.sock
#允許最大連接數
max_connections=200
#服務端使用的字符集默認為8比特編碼的latin1字符集
character-set-server=utf8
#創建新表時將使用的默認存儲引擎
default-storage-engine=INNODB
#表名存儲在磁盤是小寫的，但是比較的時候是不區分大小寫
lower_case_table_names=1
#服務所能處理的請求包的最大大小以及服務所能處理的最大的請求大小
max_allowed_packet=16M
#是否支持快捷方式
symbolic-links=0
#錯誤日誌路徑
log-error=/data/mysql/logs/mysqld.log
#pid文件所在目錄
pid-file=/data/mysql/mysqld/mysqld.pid
server-id = 1 # 表示唯一的 Server ID，串連主機中 Server ID 不可重複。
bind-address = 0.0.0.0 # 開放可以連線的 Network，主要讓 Slave 節點可以與 MySQL Master 進行連線。
innodb_flush_log_at_trx_commit = 1 # 設定 Commit 寫進磁碟的策略，1 是預設值，會盡力確保資料被寫入 Disk，雖然比較慢但是在 Replication Cluster 中格外重要，這也是 Replication 架構中官方建議的設定。
sync_binlog = 1 # 表示每次的 Transaction 都會被寫進 IO，有效避免 Crash 時造成的資料損壞，也讓最新的 Binlog 可以被即時同步到其他 Slave 上。但這個參數會嚴重影響效能，特別是在大量的 Transaction 發生時，系統使用 SSD 可以大幅改善這個 IO 效能問題，如果是在 Slave 上就不需要開啟，免得同步動作跟不上 Master。
#開啟二進制日誌功能，binlog數據位置
log-bin=/data/mysql/binlog/master-bin
#設定從資料庫可更新主資料庫的二進位檔案
log-slave-updates=true
EOF


#將data/mysql的擁有者及群組改為MySQL
chown -R mysql.mysql /data/mysql

#mysql初始化
/data/mysql/bin/mysqld --initialize --user=mysql --basedir=/data/mysql --datadir=/data/mysql/data


#設定環境變數
cat >> /etc/profile << "EOF"
MYSQL_HOME=/data/mysql
PATH=$PATH:$MYSQL_HOME/bin
EOF

###################################################################################################
#將MySQL寫進系統服務
cat > /usr/lib/systemd/system/mysqld.service << "EOF"
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=http://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql

Type=forking

PIDFile=/data/mysql/mysqld/mysqld.pid

# Disable service start and stop timeout logic of systemd for mysqld service.
TimeoutSec=0

# Execute pre and post scripts as root
PermissionsStartOnly=true

# Start main service
ExecStart=/data/mysql/bin/mysqld --daemonize --pid-file=/data/mysql/mysqld/mysqld.pid $MYSQLD_OPTS

# Sets open_files_limit
LimitNOFILE = 5000

Restart=on-failure

RestartPreventExitStatus=1

PrivateTmp=false
EOF

systemctl daemon-reload
###################################################################################################

#啟動 MySQL
systemctl start mysqld.service

#預設開機啟動 MySQL
systemctl enable mysqld.service

#master 
echo "0、請手動輸入 source /etc/profile 套用環境變數"
echo "請手動輸入 啟動 MySQL systemctl start mysqld.service"
echo "請手動輸入 預設開機啟動 MySQL systemctl enable mysqld.service"
echo "1、手動查看MySQL root預設密碼，請登入${user_name}身份"
echo "cat /data/mysql/logs/mysqld.log | grep "\"temporary password"\" "
echo "2、透過mysql -u root -p 登入，輸入舊密碼"
echo "3、變更密碼alter user 'root'@'localhost' identified by '輸入password';" 
echo "4、變更密碼後請exit退出後再重新登入" 
echo "5、設定slave連線 grant replication slave on *.* to 'myslave'@'輸入slave-ip' identified by '輸入password';"
echo "select user,host from mysql.user;"
echo "6、給伺服器授權 flush privileges;"
echo "7、查詢File、Position名稱與值 show master status;"
echo "8、master 設定完成後 驗證是否一致show databases;"
echo "9、確認無誤後exit即可退出"